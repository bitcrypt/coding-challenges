import * as t from 'io-ts'
import * as b from './brands'

const NSW = t.type({
    name: t.literal('New South Wales'),
    abbreviation: t.literal('NSW'),
})
const VIC = t.type({
    name: t.literal('Victoria'),
    abbreviation: t.literal('VIC'),
})
const QLD = t.type({
    name: t.literal('Queensland'),
    abbreviation: t.literal('QLD'),
})
const TAS = t.type({
    name: t.literal('Tasmania'),
    abbreviation: t.literal('TAS'),
})
const NT = t.type({
    name: t.literal('Northern Territory'),
    abbreviation: t.literal('NT'),
})
const WA = t.type({
    name: t.literal('Western Australia'),
    abbreviation: t.literal('WA'),
})
const SA = t.type({
    name: t.literal('South Australia'),
    abbreviation: t.literal('SA'),
})

const Result = t.exact(t.type({
    name: t.string,
    postcode: t.number,
    state: t.union([NSW, VIC, WA, QLD, TAS, NT, SA]),
    locality: t.string,
    latitude: t.union([b.Latitude, t.null]),
    longitude: t.union([b.Longitude, t.null]),
}))
export type Result = t.TypeOf<typeof Result>


export const ResultListC = t.array(Result)
export type ResultListC = t.TypeOf<typeof ResultListC>
