import iconPath from "./icons.svg";
import "./Button.css";
import * as React from "react";
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as E from 'fp-ts/lib/Either'
import { useActor } from "@xstate/react";
import { StateContext } from "../../App";


type Props = {
  className?: string
}
const Button = (props: Props) => {
  const services = React.useContext(StateContext)
  const [current, send] = useActor(services.searchService)

  const active = F.pipe(current.context.lastResult, O.fold(F.constFalse, F.constTrue))
  const className = (active ? 'Button-active ' : 'Button-inactive ') + props.className
  const alertMessage = F.pipe(current.context.lastResult, O.fold(F.constant('Never selected'), v => v.name))
  const alert = () => window.alert(alertMessage)

  return (
    <button
      disabled={!active}
      type="button"
      className={className}
      onClick={alert}
    >
      <svg viewBox="0 0 24 24" width="24" height="16">
        <use xlinkHref={iconPath + "#dls-icon-arrow-right"} />
      </svg>
    </button>
  );
}

export default Button
