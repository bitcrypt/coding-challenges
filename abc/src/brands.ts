import * as t from 'io-ts'
import * as F from 'fp-ts/lib/function'
import * as E from 'fp-ts/lib/Either'

// Custom brands that express the bounded ranges for Latitude under base10/decimal repr.
export const Latitude = t.brand(
    t.number,
    (n): n is t.Branded<number, {readonly Latitude: unique symbol}> => F.pipe(
        n,
        E.fromPredicate(v => v >= -90, F.constFalse),
        E.map(v => v <= 90),
        E.fold(F.identity, F.constTrue)
    ),
    'Latitude',
)
export type Latitude = t.TypeOf<typeof Latitude>


// Custom brands that express the bounded ranges for Longitude under base10/decimal repr.
export const Longitude = t.brand(
    t.number,
    (n): n is t.Branded<number, {readonly Longitude: unique symbol}> => F.pipe(
        n,
        E.fromPredicate(v => v >= -180, F.constFalse),
        E.map(v => v <= 180),
        E.fold(F.identity, F.constTrue)
    ),
    'Longitude',
)
export type Longitude = t.TypeOf<typeof Longitude>