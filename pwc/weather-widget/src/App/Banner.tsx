import React from 'react'
import { pipe } from 'fp-ts/lib/pipeable'
import { fold, fromPredicate } from 'fp-ts/lib/Option'

import * as a from '../automata'

import { Props, dataM, celciusToFahrenheit, getSymbol } from './'

import Weather from '../Weather'

const dateLocaleString = (current: a.CurrentState) => pipe(
  current.context.data.response,
  fold(
    () => <></>,
    r => <div>
      {new Date(r.time).toLocaleDateString('en-au', { weekday: 'long' })}, {'  '}
      {new Date(r.time).toLocaleDateString('en-au', { month: 'long' })} {'  '}
      {new Date(r.time).getFullYear()}
    </div>
  )
)

const getConditionName = (current: a.CurrentState) => pipe(
  current.context.data.response,
  fold(() => '', r => r.consolidated_weather[0].weather_state_name)
)

const getConditionWeatherGlyph = (current: a.CurrentState) => pipe(
  current.context.data.response,
  fold(
    () => <></>,
    r => <div className='w-1/6 float-left'> <Weather condition={r.consolidated_weather[0].weather_state_abbr} className='w-full' /></div>
  )
)

const getTemperature = (current: a.CurrentState, isFahrenheit: boolean) => pipe(
  current.context.data.response,
  fold(() => 0, r => r.consolidated_weather[0].the_temp),
  t => celciusToFahrenheit(isFahrenheit, t)
)

const getExtra = (current: a.CurrentState) => pipe(
  current.context.data.response,
  fold(
    () => <></>,
    r => pipe(
      r.consolidated_weather[0],
      w => 
        <table>
          <body>
            <tr>
              <td>Humidity: </td>
              <td className='pl-2'>{w.humidity}</td>
            </tr>
            <tr>
              <td>Wind: </td>
              <td className='pl-2'>{w.wind_speed.toFixed(2)} {'  '} mph {w.wind_direction_compass}</td>
            </tr>
            <tr>
              <td>Air Pressure: </td>
              <td className='pl-2'>{w.air_pressure} mbar</td>
            </tr>
            <tr>
              <td>Visibility: </td>
              <td className='pl-2'>{w.visibility.toFixed(2)} miles</td>
            </tr></body>
        </table>
    )
  )
)

export default (props: Props) => {
  const [isFahrenheit, toggleIsFahrenheit] = React.useState(false)

  const data = dataM(props.current)
  const localeString = dateLocaleString(props.current)
  const conditionName = getConditionName(props.current)
  const glyph = getConditionWeatherGlyph(props.current)
  const symbol = getSymbol(isFahrenheit)

  const temp = pipe(
    getTemperature(props.current, isFahrenheit),
    fromPredicate(v => v > 0),
    fold(
      () => <></>,
      v => <div onClick={e => toggleIsFahrenheit(!isFahrenheit)}
                className='flex text-4xl text-gray-600 ml-3 items-center cursor-pointer'>
                  {v}{symbol}
           </div>
    ))

  const extra = getExtra(props.current)


  return (
    <div>
      <div className='text-3xl bold cursor-pointer'>{data.title}</div>
      <div className='text-gray-600'>{localeString}</div>
      <div className='text-gray-600'>{conditionName}</div>
      <div className='w-full flex mb-4'>
        <div className='w-1/2 flex'>
          {glyph}
          {temp}
        </div>
        <div className='w-1/2 text-sm'>
          {extra}
        </div>
      </div>
    </div>
  )
}
