import {describe, it} from '@jest/globals'
import assert from 'assert'
import * as Either from 'fp-ts/lib/Either'

import * as b from './brands'

describe('LatLonGeoPairV', () => {
    it('canBisect', () => {
        assert.deepStrictEqual(
            Either.left('hello'),
            b.canBisect('hello'),
        )

        assert.deepStrictEqual(
            Either.right('123,'),
            b.canBisect('123,'),
        )

        assert.deepStrictEqual(
            Either.right('123,456'),
            b.canBisect('123,456'),
        )
    })


    it('isPair', () => {
        assert.deepStrictEqual(
            Either.left('hello'),
            b.isPair('hello'),
        )

        assert.deepStrictEqual(
            Either.right('123,'),
            b.isPair('123,'),
        )

        assert.deepStrictEqual(
            Either.right('123,456'),
            b.isPair('123,456'),
        )
    })

    it('isNum', () => {
        assert.deepStrictEqual(
            Either.left('hello, hello'),
            b.isNum('hello, hello'),
        )

        assert.deepStrictEqual(
            Either.left('123, hello'),
            b.isNum('123, hello'),
        )

        assert.deepStrictEqual(
            Either.left('hello, 456'),
            b.isNum('hello, 456'),
        )

        assert.deepStrictEqual(
            Either.right('123, 456'),
            b.isNum('123, 456'),
        )
    })

    it('LatLonGeoPairV', () => {
        assert.deepStrictEqual(
            false,
            b.LatLonGeoPairV('abc'),
        )

        assert.deepStrictEqual(
            true,
            b.LatLonGeoPairV('123, 456'),
        )
    })
})
