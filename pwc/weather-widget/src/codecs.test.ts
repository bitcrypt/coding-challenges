import {describe, it} from '@jest/globals'
import assert from 'assert'

import * as Either from 'fp-ts/lib/Either'
import * as c from './codecs'
import * as d from './__tests__/data'


// Source
describe('Source', () => {
    it('valid', () => {
        d.data1.sources.map(source => assert.deepStrictEqual(
            Either.right(source),
            c.Source.decode(source),
        ))
    })


    it('valid2', () => {
        d.data2.sources.map(source => assert.deepStrictEqual(
            Either.right(source),
            c.Source.decode(source),
        ))
    })
})

describe('Weather', () => {
    it('valid', () => {
        d.data1.consolidated_weather.map(weather => assert.deepStrictEqual(
            Either.right(weather),
            c.Weather.decode(weather),
        ))
    })

    it('valid2', () => {
        d.data2.consolidated_weather.map(weather => assert.deepStrictEqual(
            Either.right(weather),
            c.Weather.decode(weather),
        ))
    })
})


describe('Sources', () => {
    it('valid', () => {
        assert.deepStrictEqual(
            Either.right(d.data1.sources),
            c.Sources.decode(d.data1.sources),
        )
    })

    it('valid2', () => {
        assert.deepStrictEqual(
            Either.right(d.data2.sources),
            c.Sources.decode(d.data2.sources),
        )
    })
})

describe('ApiResponse', () => {
    it('valid', () => {
        assert.deepStrictEqual(
            Either.right(d.data1),
            c.ApiResponse.decode(d.data1),
        )
    })
})

