import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 18 18"
      {...props}
    >
      <defs>
        <path id="prefix__a" d="M0 0h18v18H0z" />
      </defs>
      <clipPath id="prefix__b">
        <use xlinkHref="#prefix__a" overflow="visible" />
      </clipPath>
      <path
        d="M14.5 3c-.213 0-.42.026-.623.062A3.995 3.995 0 0010 0a4 4 0 00-4 4 .5.5 0 01-1 0c0-.622.127-1.212.336-1.762A3.973 3.973 0 004 2a4 4 0 000 8h10.5a3.5 3.5 0 100-7"
        clipPath="url(#prefix__b)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#bebdbd"
      />
      <path
        clipPath="url(#prefix__b)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#dbbd45"
        d="M11.057 11H7.143L5 14h2.855L5 18l8-5.143H9.57z"
      />
    </svg>
  )
}

export default SvgComponent
