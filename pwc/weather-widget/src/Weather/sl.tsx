import * as React from "react"

function SvgComponent(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 18 18"
      {...props}
    >
      <defs>
        <path id="prefix__a" d="M0 0h18v18H0z" />
      </defs>
      <clipPath id="prefix__b">
        <use xlinkHref="#prefix__a" overflow="visible" />
      </clipPath>
      <path
        d="M10 13a1 1 0 11-2 0 1 1 0 112 0M6 13a1 1 0 11-2 0 1 1 0 112 0M14 13a1 1 0 11-2 0 1 1 0 112 0"
        clipPath="url(#prefix__b)"
        fill="#02afbe"
      />
      <path
        d="M14.5 4c-.213 0-.42.026-.622.062A3.995 3.995 0 0010 1a4 4 0 00-4 4 .5.5 0 01-1 0c0-.622.128-1.212.337-1.762A3.978 3.978 0 004 3a4 4 0 000 8h10.5a3.5 3.5 0 100-7"
        clipPath="url(#prefix__b)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill="#bebdbd"
      />
      <path
        d="M8 16a1 1 0 11-2 0 1 1 0 112 0M12 16a1 1 0 11-2 0 1 1 0 112 0"
        clipPath="url(#prefix__b)"
        fill="#02afbe"
      />
    </svg>
  )
}

export default SvgComponent
