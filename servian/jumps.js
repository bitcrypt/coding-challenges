// Meet Fred.  Fred can jump.

// You are allowed 2 types of jumps:
//     distance = 1
//     distance = k
    
// You are given a height, k, and a jump, j.  
// How many jumps does it take Fred to get to the height exactly?

// ie:
// height = 8
// jump = 3

// It will take 4 jumps to get to 8:
// jump 3 / 3
// jump 3 / 6
// jump 1 / 7
// jump 1 / 8

// You CANNOT jump "up" and then climb down, ie:
// jump 3 / 3
// jump 3 / 6
// jump 3 / 9
// jump (-)1 / 8


const jumps = (k, j) => {
    if (j === 1) return k
    if (j === k) return 1
    if (k < j) return k
        
    return Math.floor(k / j) + (k % j)
}