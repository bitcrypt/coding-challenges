// Given a string, find the maximum recurring character.
// If two or more characters have the same counts, return the character
// that is first alphabetically. The function is expected to return a 
// character
// 
// ie:
// raccoon
// count o = 2
// count c = 2
// 
// should return "c"


const maximumOccurringCharacter = text => {
    Object.prototype.vals = function() { return Object.values(this)}
    Array.prototype.fold = function(f) {return f(this)}

    const result = text
        .split('')
        .reduce((acc, char) => 
            !!acc[char] !== false
                ?   Object.assign(acc, {[char]: {char: char, count: acc[char].count + 1}}) 
                :   Object.assign(acc, {[char]: {char: char, count: 1}}) 
        , {})
        .vals()
        .sort((a, b) => a.count > b.count ? -1 : 1)
        .filter((obj, idx, src) => obj.count === src[0].count)
        .sort((a, b) => a.char > b.char ? 1 : -1)
        .fold(arr => arr[0])
        .char

    delete Array.prototype.fold
    delete Object.prototype.vals

    return result
}