// All of these examples were done live on syncfiddle, with the other party on 
// the end of the phone.  They could see what I typed.

// Convert the array into an object with the key being the number and the count
// being how many time it occurs

// data = [1,1,2,2,2,3,3,3]

// answer
const result = input => 
    input.reduce((acc, val) =>
        acc[val] === undefined 
            ? Object.assign(acc, {[val]: 1})
            : Object.assign(acc, {[val]: acc[val] + 1}),
        {})