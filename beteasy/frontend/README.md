# SETUP

This assumes you are using `yarn` as your package manager

- From the root directory, run `yarn`.  This will install the packages
- When the packages are installed, run `yarn start`.  This will build the application, serve it from `localhost:3000` and open a new browser window with this address.

#### Overview

- When the application initialises, an action is sent to load all of the data.
- A clock is initialised in the backend, which will load fresh data every `15 seconds`.
- You can click the beteasy icon to trigger a fresh update manually.

#### Technical Implementation

- Almost all tests are forced down to the compiler level using Typescript
- Runtime testing of types have been implemented using `ImmutableJS` with monoids as default values.
- A few manual tests have been implemented to show how to do them.  To do them all is an exercise for the reader.
- RxJS is used for managing side effect, as `Observables` are close enought to monads to be useful for this.

Tests can be run with `yarn test` from the root directory.
