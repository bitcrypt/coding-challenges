import { Record, RecordOf } from 'immutable';
import { Venue as _Venue, Empty } from "../../../@domain";


const defaultValues: _Venue = {
    Venue: Empty.STRING,
    StateCode: Empty.NULL,
    Slug: Empty.STRING,
};


export type Venue = RecordOf<_Venue>
export const Venue: Record.Factory<_Venue> = Record(defaultValues);
