import { AsyncCreators, AsyncCreatorGet, AsyncCreatorSuccess, AsyncCreatorError, AjaxResponseWithAction, AjaxErrorWithAction } from "../@types";
import actions from './actions'
import endpoints from './endpoints'


const LoadEventStart = (): AsyncCreatorGet => ({
    type: actions.load_events.START,
    url: endpoints.LOAD_CONTENT(),
})


const LoadEventSuccess = (result: AjaxResponseWithAction): AsyncCreatorSuccess => ({
    type: actions.load_events.SUCCESS,
    response: result,
    data: result.response,
    action: result.action
})


const LoadEventError = (result: AjaxErrorWithAction): AsyncCreatorError => ({
    type: actions.load_events.ERROR,
    response: result,
    error: result.response,
    action: result.action
})


export default {
    load_events: <AsyncCreators>{
        START: LoadEventStart,
        SUCCESS: LoadEventSuccess,
        ERROR: LoadEventError
    }
}
