export default {
    loaded: {
        INITIALISED: '@system/INITIALISED'
    },
    clock: {
        UPDATED: '@system/CLOCK_UPDATED'
    }
}
