import { Record, RecordOf } from 'immutable';
import { State as EventState } from '../events/@records'


type _State = {
    events: EventState
}


export type State = RecordOf<_State>
export const State: Record.Factory<_State> = Record({
    events: EventState(),
});
