import { Record, RecordOf } from 'immutable';
import { Lifecycle as _Lifecycle } from "../@types";


const defaultValues: _Lifecycle = {
    loading: false,
    success: false,
    error: false,
};

export type Lifecycle = RecordOf<_Lifecycle>
export const Lifecycle: Record.Factory<_Lifecycle> = Record(defaultValues);
