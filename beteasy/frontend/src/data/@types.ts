import { Action, Store, AnyAction } from "redux";
import { AjaxResponse, AjaxError } from "rxjs/ajax";


export type Lifecycle = {
    loading: boolean
    success: boolean
    error: boolean
}


export type AsyncActions = {
    START: string
    SUCCESS: string
    ERROR: string
}

export type AjaxResponseWithAction<A=Action & any> = AjaxResponse & {
    action: A
}

export type AjaxErrorWithAction<E=Action & any> = AjaxError & {
    action: E
}

export type AsyncCreatorGet<T = {}> = Action & T & {
    url: string,
}


export type AsyncCreatorPost<T = {}> = Action & T & {
    url: string,
    body: any
}


export type AsyncCreatorStart<T = {}> =
    | AsyncCreatorGet<T>
    | AsyncCreatorPost<T>


export type AsyncCreatorSuccess<T = any, A = Action & any> = Action<string> & {
    response: AjaxResponse
    data: T
    action: A
}


export type AsyncCreatorError<E = any, A = any> = Action<string> & {
    response: AjaxError
    error: E
    action: A
}

export type DependentAction<A = {}> = Action & A

export type DependentFunction<A = DependentAction, R={}> = (action: Action & A & any) => Action & R


export type AsyncCreators = {
    START: (...params: any) => AsyncCreatorStart
    SUCCESS: (data: AjaxResponse) => AsyncCreatorSuccess
    ERROR: (error: AjaxError) => AsyncCreatorError
}
