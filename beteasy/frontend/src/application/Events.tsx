import React from 'react'
import Moment from 'moment'

import { useSelector } from 'react-redux'
import { State } from '../data'
import { Event } from '../data/events/@records'


const EventRow = (event: Event) =>
    <tr key={event.EventID}>
        <td>{event.EventName}</td>
        <td>{event.Venue.Venue}</td>
        <td>{Moment(event.AdvertisedStartTime).fromNow()}</td>
    </tr>


export default () => {

    const events = useSelector((state: State) => state.events.data)

    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Event Venue</th>
                    <th>Starts...</th>
                </tr>
            </thead>
            <tbody>
                {events
                    .toList()
                    .map(EventRow)
                }
            </tbody>
        </table>
    )
}
