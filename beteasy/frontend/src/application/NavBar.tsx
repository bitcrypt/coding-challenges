import * as React from 'react'
import BetEasyIcon from './BetEasyIcon'

type Props = {
    onClick?: React.ReactEventHandler
}

export default ({onClick}: Props) =>
    <header className="navbar bg-beteasy-primary text-light" onClick={onClick}>
        <section className="navbar-section" />
        <section className="navbar-center bg-white">
            <BetEasyIcon width={100}/>
        </section>
        <section className="navbar-section" />
    </header>
