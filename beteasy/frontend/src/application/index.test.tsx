import {Provider} from 'react-redux'

import Enzyme from 'enzyme'
import React16Adapter from "enzyme-adapter-react-16";
import React from 'react'
import ReactDOM from 'react-dom'
import App from './index'
import Data from '../data'


Enzyme.configure({ adapter: new React16Adapter() });


it('renders without crashing', () => {
    const div = document.createElement('div');

    ReactDOM.render(<Provider store={Data.store}><App /></Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
});


it('Has the logo svg source', () => {
    const element = Enzyme.mount(<Provider store={Data.store}><App /></Provider>)
    expect(element.html()).toContain('svg')
    expect(element.html()).toContain('class="beteasy-icon"')
})
