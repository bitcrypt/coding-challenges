export type ISO8601DateString = string
export type DateSlug = string // 20181127
export type EventId = number

// Implement typed empty values for monoids
export type Empty = {
    STRING: string
    NUMBER: number
    NULL: null
}


export const Empty: Empty = {
    STRING: '',
    NUMBER: -1,
    NULL: null,
}


// Business Objects
export type Venue = {
    Venue: string
    StateCode: null
    Slug: string
}


export type EventTypeDesc =
    | Empty['STRING']
    | 'Trots'
    | 'Thoroughbred'
    | 'Greyhounds'



export type EventType = {
    EventTypeID: number
    EventTypeDesc:  EventTypeDesc
    MasterEventTypeID: number
    Slug: string
}


export type Event = {
    EventID: EventId
    MasterEventID: number
    EventName: string
    EventTypeDesc: EventTypeDesc
    MasterEventName: string
    AdvertisedStartTime: ISO8601DateString
    RaceNumber: number
    EventType: EventType
    Venue: Venue
    IsMultiAllowed: boolean
    Slug: string
    DateSlug: DateSlug
    RacingStreamAllowed: boolean
    RacingReplayStreamAllowed: boolean
}
