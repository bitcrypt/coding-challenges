﻿module Main

open System.IO
open FSharp.Data
open ConsoleTables

type XmlRace = XmlProvider<"../FeedData/Caulfield_Race1.xml">
type JsonRace = JsonProvider<"../FeedData/Wolferhampton_Race1.json">

type Horse = {
    Id: int;
    Name: string; 
    Price: decimal;
}

type Log = {
    Name: string; 
    Price: decimal;
}


let loadFile (filePath : string) =
    if not <| File.Exists(filePath) then
            failwithf "File does not exist on path %s" filePath
    File.ReadAllText(filePath)


let caulfield() = 
    let file = loadFile("../FeedData/Caulfield_Race1.xml") |> XmlRace.Parse
    let horses = file.Race.Horses 
    let prices = file.Race.Price.Horses

    prices 
    |> Seq.map (fun p -> { Horse.Id = p.Number 
                           Horse.Price = p.Price
                           Horse.Name = "" })
    |> Seq.map (fun p -> 
                    horses 
                    |> Seq.tryPick ( fun h -> if h.Number = p.Id then Some h.Name else None )
                    |> fun h -> match h with 
                                | None -> ""
                                | Some value -> value
                    |> fun name -> { p with Horse.Name=name })


let wolferhampton() =
    let file = loadFile("../FeedData/Wolferhampton_Race1.json") |> JsonRace.Parse

    file.RawData.Markets 
    |> Seq.head
    |> fun markets -> markets.Selections
    |> Seq.map (fun selection -> { Horse.Id = selection.Tags.Participant
                                   Horse.Name = selection.Tags.Name
                                   Horse.Price = selection.Price })


[<EntryPoint>]
let main _ =
    Seq.concat  [wolferhampton(); caulfield();] 
                |> Seq.sortBy ( fun horse -> horse.Price )
                |> Seq.map ( fun horse -> { Log.Price = horse.Price;
                                            Log.Name = horse.Name; })
                |> fun data -> ConsoleTable.From(data).Write()
                |> fun _ -> 0