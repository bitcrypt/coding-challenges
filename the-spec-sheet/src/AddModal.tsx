import React from 'react';
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import { Dialog, Transition } from '@headlessui/react'
import { GlobalStateContext } from './App';
import { useActor } from '@xstate/react';

export default function Modal() {
  const services = React.useContext(GlobalStateContext)
  const [current, send] = useActor(services.fileBrowserService)

  const title = F.pipe(
    current.context.add.title,
    O.fold(F.constant(''), F.identity)
  )

  const description = F.pipe(
    current.context.add.description,
    O.fold(F.constant(''), F.identity)
  )


  return (
    <Transition.Root show={current.matches('add')} >
      <Dialog as="div" className="relative z-10" onClose={() => send('TOGGLE_MODAL')}>
        <Transition.Child
          as={React.Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={React.Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white px-4 pt-5 pb-4 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-sm sm:p-6">
                <div className="space-y-6" >
                  <div>
                    <label htmlFor="title" className="block text-sm font-medium text-gray-700">
                      Title
                    </label>
                    <div className="mt-1">
                      <input
                        name="title"
                        type="text"
                        autoFocus={false}
                        className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                        onChange={e => send({ type: 'ENTER_TITLE', data: e.currentTarget.value })}
                        value={title}
                      />
                    </div>
                  </div>

                  <div>
                    <label htmlFor="content" className="block text-sm font-medium text-gray-700">
                      Content
                    </label>
                    <div className="mt-1">
                      <textarea
                        name="content"
                        className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                        onChange={e => send({ type: 'ENTER_DESCRIPTION', data: e.currentTarget.value })}
                        value={description}
                      />
                    </div>
                  </div>

                  <div>
                    <button
                      onClick={() => send('ADD_FOLDER')}
                      className="flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                      Save
                    </button>
                  </div>
                </div>
                <p className='text-center text-sm mt-2'>
                  If content is entered it is created as a file.  If no content is entered its created as a folder.
                </p>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  )
}