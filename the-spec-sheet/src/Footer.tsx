import React from 'react';
import * as F from 'fp-ts/lib/function'
import * as E from 'fp-ts/lib/Either'
import * as A from 'fp-ts/lib/Array'
import { GlobalStateContext, optEq, react_lift } from './App';
import { useActor } from '@xstate/react';

type Props = {}

const Footer = () => {
    const services = React.useContext(GlobalStateContext)
    const [current, send] = useActor(services.fileBrowserService)

    const _heirarchyResources = F.pipe(
        current.context.resources,
        A.filter(res => optEq.equals(res.parent, current.context.current))
    )

    const _searchResources = F.pipe(
        current.context.search,
        E.fromOption(F.constant(_heirarchyResources)),
        E.map(search => F.pipe(
            _heirarchyResources,
            A.filter(res => {
                const _search = search.toLowerCase()
                const _title = res.title.toLowerCase()
                return _title.includes(_search)
            }),
        )),
        E.fold(F.identity, F.identity),
    )

    const HeirarchyResources = F.pipe(_heirarchyResources, A.size, react_lift)
    const SearchResources = F.pipe(_searchResources, A.size,  react_lift)

    return (
        <footer className="bg-white" aria-labelledby="footer-heading">
            <h2 id="footer-heading" className="sr-only">
                Footer
            </h2>
            <div className="mx-auto max-w-7xl py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
                <div className="mt-8 border-t border-gray-200 pt-8 md:flex md:items-center md:justify-between">
                    <div className="flex space-x-6 md:order-2">
                    </div>
                    <div className="mt-8 text-base text-gray-400 md:order-1 md:mt-0">
                        Heirarchy Resources: <HeirarchyResources />
                    </div>
                    <div className="mt-8 text-base text-gray-400 md:order-1 md:mt-0">
                        Search Resources in current heirarchy: <SearchResources />
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer