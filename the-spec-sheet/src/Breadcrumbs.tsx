import * as React from "react"
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as A from 'fp-ts/lib/Array'
import * as E from 'fp-ts/lib/Either'

import { useActor } from "@xstate/react"

import { GlobalStateContext, optEq } from "./App"
import {
    HomeIcon,
} from '@heroicons/react/20/solid'
import { TResource } from "./machine"


const Crumb = (props: { resource: TResource, current: O.Option<number>, handler: () => void }) => (
    <li key={props.resource.title} onClick={props.handler}>
        <div className="flex items-center">
            <svg
                className="h-5 w-5 flex-shrink-0 text-gray-300"
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 20 20"
                aria-hidden="true"
            >
                <path d="M5.555 17.776l8-16 .894.448-8 16-.894-.448z" />
            </svg>
            <span
                className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700 cursor-pointer"
                aria-current={optEq.equals(O.some(props.resource.id), props.current)}>
                {props.resource.title}
            </span>
        </div>
    </li>
)


// This walks the graph to arbitrary positions to build a list of breadcrumbs. 
// It has super rad O(m x n) performace 
// fp-ts gives awesome full type safety, but in situations like this the 
// limitations of typescript syntax can be very, very clearly seen.
const recurseGraph = (r: TResource, array: Array<TResource>, resources: Array<TResource>): Array<TResource> => F.pipe(
    F.pipe(array, A.prepend(r)),                                                   // Add element to the array
    E.fromPredicate(rs => F.pipe(optEq.equals(rs[0].parent, O.none)), F.identity), // Check if parent exists
    E.fold(
        rs => F.pipe(                                                              // Find parent node and recurse 
            resources,
            A.findFirst((res: TResource) => optEq.equals(O.some(res.id), r.parent)),
            O.fold(() => rs, p => recurseGraph(p, rs, resources)),
        ),
        F.identity,
    )
)

const Bread = () => {
    const services = React.useContext(GlobalStateContext)
    const [current, send] = useActor(services.fileBrowserService)

    const crumbs = F.pipe(
        current.context.current,
        O.chain(n => F.pipe(current.context.resources, A.findFirst((res: TResource) => res.id === n))), // <--- current resource, access safe
        O.map(r => recurseGraph(r, [], current.context.resources)),                                     // <--- build the list
        O.fold(F.constant([]), F.identity),
    )

    return (
        <nav className="flex items-center space-x-4" aria-label="Breadcrumb">
            <ol role="list" className="flex items-center space-x-4">
                <li>
                    <div>
                        <div className="text-gray-400 hover:text-gray-500 flex cursor-pointer" onClick={() => send({ type: 'NAVIGATE', data: O.none })}>
                            <HomeIcon className="h-5 w-5 flex-shrink-0" aria-hidden="true" />
                            <span className="ml-3 -mt-0.5" >Home</span>
                        </div>
                    </div>
                </li>
                {crumbs.map((crumb, idx) =>
                    <Crumb 
                        resource={crumb} 
                        current={current.context.current} 
                        handler={() => send({type: 'NAVIGATE', data: O.some(crumb.id)})}
                        key={idx} />)}
            </ol>
        </nav>
    )
}

export default Bread