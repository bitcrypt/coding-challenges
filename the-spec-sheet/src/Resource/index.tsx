import React from "react";
import type { TResource } from "../machine";
import File from "./File";
import Folder from "./Folder";

export default function (resource: TResource) {
    // if you haven't seen this before this is the only way I 
    // know how to do an exhaustive pattern match in Typescript.
    // Comment out one of the case statements to see the compiler
    // whinge :P
    const _bottom = (value: never): any => void 0
    const _type = resource.type

    switch (_type) {
        case 'file':
            return <File {...resource} />

        case 'folder':
            return <Folder {...resource} />

        default:
            return _bottom(_type);
    }

}