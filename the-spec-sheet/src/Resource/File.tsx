import * as React from 'react'
import * as F from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import { DocumentIcon } from '@heroicons/react/24/outline'
import { TFile } from '../machine'
import FileModal from './FileModal'

export type Props = Pick<TFile, 'title' | 'parent' | 'description' | 'id'>

export const File = (props: Props) => {
    const parent = F.pipe(props.parent, O.fold(F.constant(O.none._tag), v => Number(v).toString()))
    const [showModal, setShowModal] = React.useState(false)
    return (
        <>
            <FileModal file={props} showModal={showModal} setShowModal={()=> setShowModal(!showModal)} />
            <section className="col-span-1 col-start-1 cursor-pointer bg-white hover:bg-gray-50 transition duration-150" 
                     onDoubleClick={() => setShowModal(true)}>
                <div className=" px-4 py-5 shadow sm:rounded-lg sm:px-6">
                    <div className="justify-stretch mt-6 flex justify-center">
                        <DocumentIcon className='w-12 h-12' />
                    </div>
                    <div className="justify-stretch mt-6 justify-center">
                        <div className='m-2 text-center'>{props.title}</div>
                        <div className='text-xs m-2 text-center'>{props.description}</div>
                        <div className='text-xs m-2'>
                            <div>ID: {props.id}</div>
                            <div>Parent: {parent}</div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default File