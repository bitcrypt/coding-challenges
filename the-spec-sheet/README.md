## File explorer thingy

`yarn start` or `docker-compose up`

### Implemented using:
- fp-ts
- xstate
- tailwind
- typescript
- react

### Design:
- `folders/files` implemented as a non-linear graph of type `TResource`. `TResource` is as a union of (and thus constructed by)  `TFile | TFolder`. 
  - Initially I structurally matched on this type
  - This worked but there is so much structural overlap this way leads to dragons
  - Ended up using them as tagged unions with a `type` key
  - If you wanted to improve this you'd use idiomatic `fp-ts` keys and construct it as `{ _tag: file | value: TFile } | { _tag: folder | value: TFolder }`.  
- If you were going to add delete functionality into this you'd turn it into a tree and balance it so you could nuke everything underneath the node
- search operation builds an index by lowercasing the filename and search term and then comparing them.
  - no clever bitshifting going on here. 
  - search time is `O(n)` 😅
  - if you want to improve this then:
    - you'd index it the resource once and trade space for time
    - you'd properly normalise the search term (ie trip trailing whitespace etc) rather than my ad-hoc lower-casing it
- No `null` values.  Everything implemented using standing `*ML` combinators, with `null` being wrapped by `O.none`

### Features not implemented:
- ~~breadcrumbs:~~
  - ~~you'd recursively walk the graph from the current node.  You know you would hit the root when you found  `Option<None>`.  Each pass would prepend to a `breadcrumbs` list which would resolve once it found its root.  Yes, this isn't stack safe.  No, I wouldn't fix it - if someone has a folder depth of ~65,000 they deserve to blow the stack~~ 
  - **now fully implemented exactly as described**
- history/back button
  - You'd push the active resource onto the history stack.  
  - You'd have a subscribe event send a message to the machine to call the `navigation` event and modify the state.
  - All access is already type safe via `Option` lookups, so if delete was implemented and you went to a missing node you would just be pushed back to the home screen 🤙
- rehydrate state
  - 👆 how awesome is technical jargon
  - you can serialise the context of the machine and push it into `localStorage`.  On init you'd try to find the state and push it into context, otherwise you'd load a default context. 
  - For the implementation attempt at this see `codecs` and `machines/lazyFileBrowserMachine`, including a detailed breakdown of squinting at the problem after a few beers 🍺😄.


### Problems I faced doing this:
The whole thing took about 5 hours.  2 of those hours were trying to understand why `Some<ActualValue> != Some<ActualValue>`.  The answer is, of course, that `Object != Object` because Javascript compares by heap/memory id, not value. I've been doing this for over a decade and still forget this sometimes 🤦‍♂️ reee