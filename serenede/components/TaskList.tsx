import {useEffect, useState, useCallback} from "react";
import TaskCard from "./TaskCard";

import styles from './TaskList.module.css';
import AddTask from "./AddTask";

type Results = {
  size: number,
  from: number,
  page: number,
  results: Array<Task>
}

const DefaultResults:Results = ({
  size: 0,
  from: 0,
  page: 0,
  results: [],
})

const TaskList = () => {
    const [tasks, setTasks] = useState<Results>(DefaultResults);
    const [offset, setOffSet] = useState<number>(0)
    
    // We have 2 broad ways to deal with the delete here.  We can either remove the
    // task from the list locally, or we can trigger a refresh from the server and 
    // update it that way.
    // In a multi-user task list you should really fetch the updated tasklist from
    // the remote host, that way your data is "less stale".  In a single-user task
    // list it shouldn't matter, so I'm going to modify it in place rather than 
    // doing a second call.

    const deleteTask = (deletedTask: Task) => {
      const newTasks = tasks.results.filter(task => task.id !== deletedTask.id)
      tasks.results = newTasks
      tasks.size =- newTasks.length
      setTasks({...tasks})
    }
    
    // Normally you'd do this with a lens from something like monocle-ts for all of
    // that declarative goodness, but here we'll do it with an imperative update
    
    const taskAdded = (task: Task) => {
      tasks.results = tasks.results.concat([task]);
      tasks.size += 1;
      setTasks({...tasks})
    }
    
    // I burned way too much time trying to figure out why this wasn't rerendered. 
    // I forgot that React compares on memory/heap id, so updating an object didn't
    // cause the rerender because the address was still the same.  Derp.
    const loadMore = async () => {
      setOffSet(offset + 5)
      await fetch('/api/tasks', {
            method: 'POST',
            body: JSON.stringify({
                from: offset,
                size: 5,
            })
        })
        .then(r => r.json())
        .then(r => r.items)
        .then(r => {
          tasks.size = r.size
          tasks.results = tasks.results.concat(r.results)
          setTasks({...tasks})
        })
    }
    
    

    useEffect(() => {
      fetch('/api/tasks', {
            method: 'POST',
            body: JSON.stringify({
                from: 0,
                size: 5,
            })
        })
            .then(r => r.json())
            .then(r => setTasks(r.items))
            .catch(console.log);
    }, []);

    return (
        <div className={styles.taskList}>
            {tasks.results.map(task => <TaskCard key={task.id} task={task} deleteTask={deleteTask} />)}
            <AddTask onTaskAdded={taskAdded}/>
            <div style={{display: tasks.size > tasks.results.length ? 'block' : 'none'}}> 
              <button onClick={loadMore}>Load More ({tasks.results.length} Loaded, {tasks.size} Total)</button>
            </div>
        </div>
    )
}

export default TaskList;