import Persona from 'persona';
import { Machine, MachineConfig, assign, EventObject, Interpreter } from 'xstate'

type AutomataContext = {
    authentication: {
        ssoCode: string,
        token: string,
        uuid: string
    },
    registration: {
        firstname: string
        lastname: string
        email: string
        password: string
    }
    login: {
        username: string
        password: string
    }
    profile: {
        address: {
            countryCode: string,
            zipCode: string,
            streetAddress: string,
        }
        consent: Array<any>
        cusno: string
        email: string
        firstName: string
        lastName: string
        legal: Array<{
            consentId: string
            dateAccepted: string
            screenName: string
        }>
        pendingAddressChanges: any
        subs: Array<any>
        uuid: string
    }
}

type AutomataStateSchema = {
    states: {
        login: {}
        submit_login: {}
        login_error: {}
        login_success: {}

        register_new_user: {}
        submit_new_user: {}
        submit_new_user_success: {},
        submit_new_user_error: {}

        load_profile_data: {}
        load_profile_data_error: {}
        profile_page: {}

        update_address: {}
        submit_address: {},
        submit_address_error: {},

    }
}

type AutomataEvent =
    | { type: 'TO_REGISTER_NEW_USER' }
    | { type: 'REGISTER_NEW_USER_DETAILS' }
    | { type: 'SUBMIT_NEW_USER_DETAILS' }
    | { type: 'TO_LOGIN' }
    | { type: 'ENTER_LOGIN_DETAILS' }
    | { type: 'LOGIN_START' }
    | { type: 'LOGIN_SUCCESS' }
    | { type: 'LOGIN_ERROR' }
    | { type: 'UPDATE_ADDRESS' }
    | { type: 'SUBMIT_ADDRESS' }
    | { type: 'SUBMIT_ADDRESS_ERROR' }
    | { type: 'TO_PROFILE' }
    | { type: 'RETRY' }

export type AutomataService = Interpreter<AutomataContext, AutomataStateSchema, AutomataEvent>

let UsersApi = new Persona.UsersApi();
let LoginApi = new Persona.LoginApi();
UsersApi.apiClient.basePath = "https://persona.api.ksfmedia.fi/v1"
LoginApi.apiClient.basePath = "https://persona.api.ksfmedia.fi/v1"

// Due to a bug in the way the Persona module has been implemented, we need to wrap the
// entire thing as a promise and pass a generic resolve/reject callback to the library
// callbacks
const promiseCallback = (resolve: any, reject: any) => (error: any, success: any) =>
    error === null
        ? resolve(success)
        : reject(error)


// Machine Services
const submitUserLogin = (c: AutomataContext, e: AutomataEvent) => new Promise((res, rej) =>
    LoginApi.loginPost(
        new Persona.LoginData(
            c.login.username,
            c.login.password), promiseCallback(res, rej)))


const submitNewUser = async (c: AutomataContext) => new Promise((res, rej) =>
    UsersApi.usersPost(
        new Persona.NewUser(
            c.registration.firstname,
            c.registration.lastname,
            c.registration.email,
            c.registration.password,
            c.registration.password), promiseCallback(res, rej)))

const loadProfileData = (c: AutomataContext, e: AutomataEvent) => new Promise((res, rej) =>
    UsersApi.usersUuidGet(
        c.authentication.uuid,
        { 'authorization': 'OAuth ' + c.authentication.token }, promiseCallback(res, rej)))

const submitAddress = (c: AutomataContext, e: AutomataEvent) => new Promise((res, rej) =>
    UsersApi.usersUuidPatch(
        c.authentication.uuid,
        c.profile,
        { 'authorization': 'OAuth ' + c.authentication.token },
        promiseCallback(res, rej)))

// Machine Actions
const assignLoginData = assign({
    login: (c: any, e: EventObject) => Object.assign(c.login, { [e.field]: e.value })
})
const assignAuthenticationToken = assign({
    authentication: (c: any, e: EventObject) => ({
        token: e.data.token,
        uuid: e.data.uuid,
        ssoCode: e.data.ssoCode,
    })
})
const assignRegistrationData = assign({
    registration: (c: any, e: EventObject) => Object.assign(c.registration, { [e.field]: e.value })
})
const assignProfileData = assign({
    profile: (c: any, e: EventObject) => {
        const data = e.data.address !== undefined
            ? e.data
            : Object.assign(e.data, {address: {
                zipCode: '',
                countryCode: '',
                streetAddress: '',
            }})

        return Object.assign(c.profile, { ...data })
    }
})
const assignAddressData = assign({
    profile: (c: any, e: EventObject) => {
        const address = Object.assign(c.profile.address, { [e.field]: e.value })
        return Object.assign(c.profile, {address})
    }
})

const config: MachineConfig<AutomataContext, AutomataStateSchema, AutomataEvent> = {
    id: 'base_machine',
    initial: 'login',
    context: {
        authentication: {
            token: '',
            ssoCode: '',
            uuid: ''
        },
        registration: {
            firstname: '',
            lastname: '',
            email: '',
            password: ''
        },
        login: {
            username: '',
            password: '',
        },
        profile: {
            address: {
                countryCode: '',
                zipCode: '',
                streetAddress: ''
            },
            consent: [],
            cusno: '',
            email: '',
            firstName: '',
            lastName: '',
            legal: [],
            pendingAddressChanges: [],
            subs: [],
            uuid: ''
        }
    },
    states: {
        login: {
            on: {
                TO_REGISTER_NEW_USER: {
                    target: 'register_new_user'
                },
                ENTER_LOGIN_DETAILS: {
                    actions: ['assignLoginData']
                },
                LOGIN_START: {
                    target: 'submit_login'
                }
            }
        },
        submit_login: {
            invoke: {
                src: 'submitUserLogin',
                onDone: {
                    target: 'login_success',
                    actions: 'assignAuthenticationToken'
                },
                onError: {
                    target: 'login_error'
                }
            }
        },
        login_error: {
            on: {
                LOGIN_START: {
                    target: 'login'
                }
            }
        },
        login_success: {
            after: {
                500: {
                    target: 'load_profile_data'
                }
            }
        },
        register_new_user: {
            on: {
                TO_LOGIN: {
                    target: 'login'
                },
                REGISTER_NEW_USER_DETAILS: {
                    actions: ['assignRegistrationData']
                },
                SUBMIT_NEW_USER_DETAILS: {
                    target: 'submit_new_user'
                }
            }
        },
        submit_new_user: {
            invoke: {
                src: 'submitNewUser',
                onDone: {
                    target: 'login'
                },
                onError: {
                    target: 'submit_new_user_error'
                }
            }
        },
        submit_new_user_success: {
            on: {
                TO_LOGIN: {
                    target: 'login'
                }
            }
        },
        submit_new_user_error: {
            on: {
                TO_LOGIN: {
                    target: 'login'
                },
                SUBMIT_NEW_USER_DETAILS: {
                    target: 'submit_new_user'
                }
            }

        },
        load_profile_data: {
            invoke: {
                src: 'loadProfileData',
                onDone: {
                    target: 'profile_page',
                    actions: ['assignProfileData']
                },
                onError: {
                    target: 'load_profile_data_error'
                }
            }
        },
        load_profile_data_error: {
            on: {
                RETRY: {
                    target: 'load_profile_data',
                },
                TO_LOGIN: {
                    target: 'login'
                }
            }
        },
        profile_page: {
            on: {
                UPDATE_ADDRESS: {
                    target: 'update_address'
                }
            }
        },
        update_address: {
            on: {
                TO_PROFILE: {
                    target: 'profile_page'
                },
                UPDATE_ADDRESS: {
                    actions: ['assignAddressData']
                },
                SUBMIT_ADDRESS: {
                    target: 'submit_address'
                }
            }
        },
        submit_address: {
            invoke: {
                src: 'submitAddress',
                onDone: {
                    target: 'profile_page'
                },
                onError: {
                    target: 'submit_address_error'
                }
            }
        },
        submit_address_error : {
            on: {
                RETRY: 'submit_address',
                TO_PROFILE: 'profile_page',
                UPDATE_ADDRESS: 'update_address',
                TO_LOGIN: 'login'
            }
        }
    }
}

const options: any = {
    services: {
        submitUserLogin,
        submitNewUser,
        submitAddress,
        loadProfileData,
    },
    actions: {
        assignLoginData,
        assignAuthenticationToken,
        assignRegistrationData,
        assignProfileData,
        assignAddressData,
    }
}

export default Machine<AutomataContext, AutomataStateSchema, AutomataEvent>(config, options)
