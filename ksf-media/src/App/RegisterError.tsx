import React from 'react'
import { AutomataService } from '../automata'

type RegisterError = {service: AutomataService}
const RegisterError = ({service}: RegisterError) =>
    <div>
        There was an error registering the user.
        <div>
            <button onClick={() => service.send('SUBMIT_NEW_USER_DETAILS')}>Try again</button>
        </div>
        <div>
            <a href="#" onClick={() => service.send('TO_LOGIN')}>Back to Login</a>
        </div>
    </div>

export default RegisterError
