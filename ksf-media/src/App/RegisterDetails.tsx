import React from 'react'
import { AutomataService } from '../automata'

const changeHandler = (service: AutomataService, field: string) => (e: React.FormEvent<HTMLInputElement>) =>
    service.send('REGISTER_NEW_USER_DETAILS', {field: field, value: e.currentTarget.value})

type RegisterDetails = {service: AutomataService}
const RegisterDetails = ({service}: RegisterDetails) =>
    <div>
        <div>
            First Name: <input type="text" onChange={changeHandler(service, 'firstname')} />
        </div>
        <div>
            Last Name: <input type="text" onChange={changeHandler(service, 'lastname')} />
        </div>
        <div>
            Email: <input type="email" onChange={changeHandler(service, 'email')} />
        </div>
        <div>
            Password: <input type="text" onChange={changeHandler(service, 'password')} />
        </div>

        <div>
            <button onClick={() => service.send('SUBMIT_NEW_USER_DETAILS')}>Submit</button>
        </div>
        <div>
            <a href="#" onClick={() => service.send('TO_LOGIN')}>Back to Login</a>
        </div>
    </div>

export default RegisterDetails
