import React from 'react'
import { AutomataService } from '../automata'

type ProfileUpdateError = { service: AutomataService }
const ProfileUpdateError = ({ service }: ProfileUpdateError) => {
    return (
        <div>
            We've done a terrible thing and encountered an error while updating.
            <div>
                <button onClick={() => service.send('RETRY')}>Retry</button>
                <button onClick={() => service.send('TO_PROFILE')}>Profile Page</button>
                <button onClick={() => service.send('UPDATE_ADDRESS')}>Back to edit page</button>
                <button onClick={() => service.send('TO_LOGIN')}>Leave this nightmare (exit)</button>
            </div>
        </div>
    )
}

export default ProfileUpdateError
