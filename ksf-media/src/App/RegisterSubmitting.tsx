import React from 'react'
import { AutomataService } from '../automata'

const changeHandler = (service: AutomataService, field: string) => (e: React.FormEvent<HTMLInputElement>) =>
    service.send('REGISTER_NEW_USER_DETAILS', {field: 'firstname', value: e.currentTarget.value})

type RegisterSubmitting = {service: AutomataService}
const RegisterSubmitting = ({service}: RegisterSubmitting) =>
    <div>
        Submitting new user details....
    </div>

export default RegisterSubmitting
