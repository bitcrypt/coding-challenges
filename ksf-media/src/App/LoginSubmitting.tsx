import React from 'react'
import { AutomataService } from '../automata'

type LoginSubmitting = {service: AutomataService}
const LoginSubmitting = ({service}: LoginSubmitting) =>
    <div>
        Trying to log you in now...
    </div>

export default LoginSubmitting
