import React from 'react'
import { AutomataService } from '../automata'

type ProfileError = {service: AutomataService}
const ProfileError = ({service}: ProfileError) =>
    <div>
        Whoops!! There was an error loading your profile data :(
        <div>
            <button onClick={() => service.send('RETRY')}>Try again</button>
        </div>
        <div>
            <a href="#" onClick={() => service.send('TO_LOGIN')}>Back to Login</a>
        </div>
    </div>

export default ProfileError
