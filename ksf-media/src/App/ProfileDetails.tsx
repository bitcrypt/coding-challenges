import React from 'react'
import { AutomataService } from '../automata'
import { useService } from '@xstate/react'

type ProfileDetails = {
    service: AutomataService,
}
const ProfileDetails = ({ service }: ProfileDetails) => {
    const [current, send] = useService(service)
    const address = current.context.profile.address

    return (
        <div>
            Your Profile
        <div>
                <div>First Name: {service.state.context.profile.firstName}</div>
                <div>Last Name: {service.state.context.profile.lastName}</div>
                <div>Email: {service.state.context.profile.email}</div>
                <div>Customer Number: {service.state.context.profile.cusno}</div>
            </div>

            <h6>Address</h6>
            <div>
                <span>Country Code {' '}</span>
                <span>{address.countryCode}</span>
            </div>
            <div>
                <span>Street Address {' '}</span>
                <span>{address.streetAddress}</span>
            </div>
            <div>
                <span>Zip Code {' '}</span>
                <span>{address.zipCode}</span>
            </div>

            <div>
                <button onClick={() => service.send('UPDATE_ADDRESS')}>Update Address</button>
            </div>
        </div>
    )
}
export default ProfileDetails
