import React from 'react'
import { useService } from '@xstate/react'
import { AutomataService } from '../automata'

const changeHandler = (service: AutomataService, field: string) => (e: React.FormEvent<HTMLInputElement>) =>
    service.send('UPDATE_ADDRESS', {field: field, value: e.currentTarget.value})

type ProfileUpdate = { service: AutomataService }
const ProfileUpdate = ({ service }: ProfileUpdate) => {
    const [current, send] = useService(service)
    const address = current.context.profile.address

    return (
        <div>
            <h3>Your Profile</h3>
            <div>
                <div>First Name: {service.state.context.profile.firstName}</div>
                <div>Last Name: {service.state.context.profile.lastName}</div>
                <div>Email: {service.state.context.profile.email}</div>
                <div>Customer Number: {service.state.context.profile.cusno}</div>
                <h6>Address</h6>
                <div>
                    <span>Country Code</span>
                    <input type="text"
                        onChange={changeHandler(service, 'countryCode')}
                        value={address.countryCode}
                    />
                </div>
                <div>
                    <span>Street Address</span>
                    <input type="text"
                        onChange={changeHandler(service, 'streetAddress')}
                        value={address.streetAddress}
                    />
                </div>
                <div>
                    <span>Zip Code</span>
                    <input type="text"
                        onChange={changeHandler(service, 'zipCode')}
                        value={address.zipCode}
                    />
                </div>
            </div>

            <div>
                <button onClick={() => service.send('TO_PROFILE')}>Back to Profile</button>
                <button onClick={() => service.send('SUBMIT_ADDRESS')}>Update Address</button>
            </div>
        </div>
    )
}

export default ProfileUpdate
