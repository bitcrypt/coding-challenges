/* config-overrides.js */
module.exports = function override(config, env) {
    //do stuff with the webpack config...
    console.log(config.module.rules);
    config.module.rules.push(
        {
            parser: { amd: false }
        }
    );
    return config;
}
