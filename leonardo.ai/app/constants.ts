// This represents the primary key of a user that has been selected.
// 0 is a perfect sentinal for a database key as its an invalid value
export const nonSelectedUserId = 0
