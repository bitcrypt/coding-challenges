import { Inter } from "next/font/google";
import { initAction } from './actions'
import InnerLayout from '@/components/InnerLayout'
import { Metadata } from "next";
import "./globals.css";


const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Leonardo.ai",
  description: "Tech Test",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  await initAction()

  return (
    <html lang="en">
      <body className={inter.className}>
        <InnerLayout>{children}</InnerLayout>
      </body>
    </html>
  );
}
