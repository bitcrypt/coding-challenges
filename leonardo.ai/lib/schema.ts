import { serial, text, pgTable } from "drizzle-orm/pg-core"
import { InferSelectModel, InferInsertModel } from "drizzle-orm"


export const UsersTable = pgTable("users", {
  id: serial("id").primaryKey(),
  // Unfortunately drizzle doesn't have check constraints built in at the 
  // time of writing this, as typically you'd include something like this
  // to validate emails going into the database
  //    ^[^@\s]+@([^@\s]+\.)+\w+$ (optionally with {255} for length)
  email: text("email").notNull().unique(),
  position: text("position").notNull(),
});


export type SelectUser = InferSelectModel<typeof UsersTable>
export type InsertUser = InferInsertModel<typeof UsersTable>
