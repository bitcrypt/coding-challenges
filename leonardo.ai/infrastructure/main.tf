terraform {
  required_providers {
    vercel = {
      source  = "vercel/vercel"
      version = "~> 0.16"
    }
  }
}


resource "vercel_project" "leonardoai" {
  name          = "leonardoai-tech-test"
  framework     = "nextjs"
  build_command = "npm run migrate:prod && npm run build"
  git_repository = {
    type = "gitlab"
    repo = "mcspud/coding-challenges"
    production_branch = "master"
  }
}
