"use client"

import { Flex, Spinner } from "@chakra-ui/react";
import { useFormStatus } from "react-dom";


export default function FormLoader() {
  // Unfortunately due to a limitation in the `useFormStatus` hook we need to 
  // essentially redeclare this to get the behavior we want.
  // see https://react.dev/reference/react-dom/hooks/useFormStatus#useformstatus-will-not-return-status-information-for-a-form-rendered-in-the-same-component
  const status = useFormStatus()

  return (
    <Flex justifyContent='center' hidden={status.pending === false}>
      <Spinner color='red.500' size="xl" />
    </Flex>
  )
}

