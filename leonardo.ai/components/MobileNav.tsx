import {
  CloseButton,
  Flex,
  Text,
  Drawer,
  DrawerContent,
  BoxProps,
  UseDisclosureReturn,
} from '@chakra-ui/react'
import {
  FiHome,
  FiTrendingUp,
} from 'react-icons/fi'
import NavItem from './NavItem'
import { ActiveUserContext } from './InnerLayout'
import { nonSelectedUserId } from '@/app/constants'
import { useContext } from 'react'

interface MobileNavProps extends BoxProps {
  disclosure: UseDisclosureReturn
}
export default function MobileNav({ disclosure }: MobileNavProps) {

  const { currentUser } = useContext(ActiveUserContext)
  return (
    <Drawer
      isOpen={disclosure.isOpen}
      placement="left"
      onClose={disclosure.onClose}
      returnFocusOnClose={false}
      onOverlayClick={disclosure.onClose}>
      <DrawerContent>
        <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
          <Text fontSize="2xl" fontFamily="monospace" fontWeight="bold">
            Leonardo.ai
          </Text>
          <CloseButton display={{ base: 'flex', md: 'none' }} onClick={disclosure.onClose} />
        </Flex>
        <NavItem isDisabled={false} icon={FiHome} path={'/'}>Users</NavItem>
        {/* If there is no current user (represented by the sential value) then disable access to the info page*/}
        <NavItem isDisabled={currentUser === nonSelectedUserId} icon={FiTrendingUp} path={'/information'}>Information</NavItem>
      </DrawerContent>
    </Drawer>
  )
}
