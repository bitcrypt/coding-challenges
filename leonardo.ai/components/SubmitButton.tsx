import { Button } from "@chakra-ui/react"
import { useFormStatus } from "react-dom"


export default function SubmitButton() {
  // Unfortunately due to a limitation in the `useFormStatus` hook we need to 
  // essentially redeclare a button to get "disable" behavior.
  // see https://react.dev/reference/react-dom/hooks/useFormStatus#useformstatus-will-not-return-status-information-for-a-form-rendered-in-the-same-component
  const status = useFormStatus()

  return (
    <Button colorScheme="green" type='submit' isDisabled={status.pending === true}>Submit</Button>
  )
}
