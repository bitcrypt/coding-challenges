from django.contrib.auth import get_user_model
from rest_framework import viewsets, mixins, authentication, permissions
from . import serializers



class ProfileViewSet(mixins.UpdateModelMixin,
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    lookup_field = 'id'
    serializer_class = serializers.ProfileSerializer
    authentication_classes = [authentication.SessionAuthentication] 
    permission_classes = [permissions.IsAuthenticated] 

    def get_queryset(self, **kwargs):
        return get_user_model().objects.filter(id=self.request.user.id)

