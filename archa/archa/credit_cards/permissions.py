from rest_framework import permissions, exceptions


class IsCompanyAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        if obj.user == request.user:
            raise exceptions.PermissionDenied("Cannot modify the limit of your own card")

        return request.user in obj.company.admin_set.all()