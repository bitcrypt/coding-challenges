import random
import factory

from accounts.factories import UserFactory
from archa.faker import fake
from company.factories import CompanyFactory

from . import models


class CreditCardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.CreditCard

    number = factory.Faker('credit_card_number')
    expiry_date = factory.Faker('credit_card_expire')
    company = factory.SubFactory(CompanyFactory)
    user = factory.SubFactory(UserFactory)
    limit = factory.LazyAttribute(lambda o: random.randint(2000, 9999))


class TransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Transaction

    credit_card = factory.SubFactory(CreditCardFactory) 
    amount = factory.LazyFunction(
        lambda: fake.pydecimal(right_digits=2, positive=True, min_value=1, max_value=100)
    )
