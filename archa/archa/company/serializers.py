from rest_framework import serializers


class NestedUserSerializer(serializers.Serializer):
    username = serializers.CharField()
    email = serializers.CharField()


class NestedCardSerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='creditcard-detail', 
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField()
    limit = serializers.DecimalField(decimal_places=2, max_digits=10)
    balance = serializers.DecimalField(decimal_places=2, max_digits=10)
    user = NestedUserSerializer(read_only=True)


class CompanySerializer(serializers.Serializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='company-detail', 
        lookup_field='identifier',
    )
    identifier = serializers.UUIDField(read_only=True)
    name = serializers.CharField()
    credit_cards = NestedCardSerializer(
        many=True, 
        read_only=True,
        source='creditcard_set',
    )
