from . import models, serializers
from rest_framework import viewsets, mixins, authentication, permissions



class CompanyViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    lookup_field = 'identifier'
    serializer_class = serializers.CompanySerializer
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated] 

    def get_queryset(self, **kwargs):
        qs1 = models.Company.objects.filter(id__in=self.request.user.admin_set.all().values("id"))
        qs2 = models.Company.objects.filter(id__in=self.request.user.user_set.all().values("id"))
        return models.Company.objects.filter(id__in=qs1.union(qs2).values('id'))