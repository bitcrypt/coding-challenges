from rest_framework import routers
from accounts.viewsets import ProfileViewSet
from company.viewsets import CompanyViewSet
from credit_cards.viewsets import CardViewSet, CreditCardViewSet, TransactionViewSet

router = routers.DefaultRouter()
router.register(r'me', ProfileViewSet, basename='me')
router.register(r'card', CardViewSet, basename='card')
router.register(r'cards', CreditCardViewSet, basename='creditcard')
router.register(r'companies', CompanyViewSet, basename='company')
router.register(r'transactions', TransactionViewSet, basename='transaction')

urlpatterns = router.urls