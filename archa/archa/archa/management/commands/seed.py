from django.core.management.base import BaseCommand, CommandError
from accounts.factories import UserFactory
from company.factories import CompanyFactory
from credit_cards.factories import TransactionFactory, CreditCardFactory

class Command(BaseCommand):
    help = 'Seeds the DB with some basic data'

    def handle(self, *args, **options):

        print('Creating scenario 1 records...')
        user_1a = UserFactory(username='user_1a')
        user_1a.set_password('password')
        user_1a.save()

        user_1b = UserFactory(username='user_1b')
        user_1b.set_password('password')
        user_1b.save()

        company_1 = CompanyFactory()
        company_1.admin_set.add(user_1a)
        company_1.user_set.add(user_1b)

        card_1a = CreditCardFactory(
            user=user_1a,
            company=company_1,
        )

        card_1b = CreditCardFactory(
            user=user_1b,
            company=company_1,
        )

        TransactionFactory(credit_card=card_1a)
        TransactionFactory(credit_card=card_1a)
        TransactionFactory(credit_card=card_1a)

        TransactionFactory(credit_card=card_1b)
        TransactionFactory(credit_card=card_1b)
        TransactionFactory(credit_card=card_1b)
        TransactionFactory(credit_card=card_1b)
        TransactionFactory(credit_card=card_1b)


        print('Creating scenario 2 records...')
        user_2a = UserFactory(username='user_2a')
        user_2a.set_password('password')
        user_2a.save()

        user_2b = UserFactory(username='user_2b')
        user_2b.set_password('password')
        user_2b.save()

        company_2a = CompanyFactory()
        company_2a.user_set.add(user_2a)

        company_2b = CompanyFactory()
        company_2b.admin_set.add(user_2a)
        company_2b.user_set.add(user_2b)

        card_2a = CreditCardFactory(
            user=user_2a,
            company=company_2a,
        )

        card_2b = CreditCardFactory(
            user=user_2b,
            company=company_2b,
        )

        TransactionFactory(credit_card=card_2a)
        TransactionFactory(credit_card=card_2a)
        TransactionFactory(credit_card=card_2b)