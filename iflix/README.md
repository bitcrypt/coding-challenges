# iFlix Test

You'll need a newish `node` and `mongodb` installed, and this assumes `yarn` as the package manager.

#### Setup

Open a terminal and navigate to the root of the project

```
cd backend  
yarn            # installs packages  
yarn populate   # creates some dummy data
yarn start  
```

A node process will start binding to 7774

Open a new terminal and navigate to the root of the project

```
cd frontend  
yarn            # installs packages  
yarn start  
```

A new window should open on localhost binding to 3000.

Click the **iflix** logo to get started.

#### Some Notes

- The backend and frontend tests aren't consistent.  The frontend says to submit ratings 1-5, whilst the backend shows an example between 1-10.  I only used the 1-5 range for this.
- Because I had a small range to work over (1-5), there is no way to reliably show outliers or similar, so this has been ignored.
- I didn't write any tests for the backend.
- I only wrote 2 test files for the front end.  One shows how the decoupling of the state from the application looks (`data/content/reducers.test.tsx`), and the other shows some general usage of tests (`application/index.test.tsx`), including testing helper functions and checking that a react element has rendered correctly
- I erred on the side of caution with comments.  Anyone that has programmed in statically typed functional languages for more than about 10 minutes understands that `Types` are **immensely** more valuable than comments, short of high level overviews of the system.  In keeping with that mindset, the entire domain knowledge has been encoded into the types.

A **README** for backend and frontend can be found in each package respectively.
