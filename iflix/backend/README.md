# iFlix Ratings

This implements a backend microservice for ratings for iflix content.

The following constraints applied:

- High read throughput
- Robust data storage

Given the existing use of NodeJS on the iflix backend, NodeJS was chosen
due to its familiarity with the rest of the team, good tooling from AWS
and its relatively quick development cycle.

## Overview

iFlix has hundreds of thousands of pieces of content, and millions of users.
User rated content is a valuable feedback mechanism for the content we display, including in features like "recommendations" and "similar", along with more obvious search criteria such as "top rated", or "top movies this month".

To facilitate this, the following top level decisions were made

- Split this feature off a microservice, reducing code bloat on the primary backend and
allowing specialised API's to be developer for multiple services
- Implement MongoDB as a persistance layer.

## Implementation

### Types

Typescript is used extensively in this project.  Infact, the entire project is driven by types specified in the `@domain.ts` file at the top of the project.  Everything from validator constraints to the `mongodb schema` are enforced with these types.

This will need to be optimised for `read` access.  The recommended approach is that the precomputed rating be flushed to a redis cache.  Depending on activity, this can be done either as a scheduled-task type implementation (think cronjob) or on every update.

#### Precomputed Ratings/Scores

Given the potentially vast amounts of ratings, it is technically infeasable to compute a "fresh" average every access point.  To solve this, every time a new rating is added to the collection, an associated aggregate entry is updated in the `scores` collection.  This document accretes values - ie it keeps a running total of all reviews and a running total of the amounts of reviews.  This means that every time a review is added, the count is increased by 1 and the total by N, and the new average computed by diving `total` by `count`.

## Future Optimisations

Redis has a far faster lookup time than mongodb.  By having precomputed totals, we can use this as a cache layer and have servers access this directly.  In terms of a cache rebuild we can then flush the new totals to the redis layer automatically.

### Known Issues and Undefined Requirements

##### MongoDB Race Conditions

We are unable to run transactions or "trigger-like" things on a mongo database.  Since the `scores` documents are dependent on the ratings collections, it is likely over time that due to latency we will have a divergence issue.  For example, imagine we have 2 users that have both submitted a `rating` document:

- User one updates a `score` document, with a current total of 100
- User two updates the same `score` document, which due to network latency they see as `100` also
- User one submits their score, `100 + 5`
- User two submits their score, `100 + 5`

User two overwrites the score of User one, a classic race condition.

##### Cache Synchronising

Due to the race conditions above, the scores cache will need to be periodicially rebuilt to reflect the correct rating of a movie.
