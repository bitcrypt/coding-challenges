import mongoose from 'mongoose'
import config from './config'
import { ApplicationConfiguration } from './@types';

type Database =
    | ApplicationConfiguration['MONGO_RATING_DATABASE']

export const connector = (database: Database) => (): Promise<mongoose.Mongoose> =>
    mongoose.connect(`mongodb://${config.MONGO_HOST}:${config.MONGO_PORT}/${database}`, {useNewUrlParser: true});

export default {
    RATINGS_DB: connector(config.MONGO_RATING_DATABASE),
}
