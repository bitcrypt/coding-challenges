import { ModelDefinition, BaseModel, SchemaMeta} from "./@types"
import { Rating, Collections } from '../@domain'
import * as mongoose from 'mongoose'

const definition: ModelDefinition<Rating> = {
    userId: { type: Number, required: true, index: true  },
    contentId: { type: Number, required: true, index: true  },
    rating: { type: Number, required: true, index: true  },
};

const schema: mongoose.Schema = new mongoose.Schema(definition, BaseModel);
const model: mongoose.Model<Rating & mongoose.Document> = mongoose.model<Rating & mongoose.Document>(Collections.RATINGS, schema);

export default <SchemaMeta<Rating>>{
    schema,
    model,
}
