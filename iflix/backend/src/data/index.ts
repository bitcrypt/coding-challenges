import content from './content'
import ratings from './ratings'
import scores from './scores'

export default {
    content,
    ratings,
    scores,
}
