import { ModelDefinition, BaseModel, SchemaMeta } from "./@types"
import { Content, Collections } from '../@domain'
import * as mongoose from 'mongoose'

const definition: ModelDefinition<Content> = {
    contentId: { type: Number, required: true, unique: true },
    title: { type: String, required: true, index: true },
    genre: { type: String, required: true, index: true },
};


const schema: mongoose.Schema = new mongoose.Schema(definition, BaseModel);
const model = mongoose.model<Content & mongoose.Document>(Collections.CONTENT, schema);

export default <SchemaMeta<Content>>{
    schema,
    model,
}
