import { ModelDefinition, BaseModel, SchemaMeta } from "./@types"
import { Score, Collections } from '../@domain'
import * as mongoose from 'mongoose'

const definition: ModelDefinition<Score> = {
    contentId: { type: Number, required: true, unique: true },
    count: { type: Number, required: true, index: false },
    score: { type: Number, required: true, index: false },
    total: { type: Number, required: true, index: false },
};

const schema: mongoose.Schema = new mongoose.Schema(definition, BaseModel);
const model = mongoose.model<Score & mongoose.Document>(Collections.SCORES, schema);

export default <SchemaMeta<Score>>{
    schema,
    model,
}
