export default {
    content: {
        GET: '/content',
    },

    rating: {
        POST: '/rating',
        GET: '/rating/:contentId',
    }
}
