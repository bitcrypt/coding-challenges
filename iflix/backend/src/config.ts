import { ApplicationConfiguration } from './@types';
import dotenv from 'dotenv'

dotenv.config()

const generateConfig = (): ApplicationConfiguration => ({
        MONGO_HOST: process.env.MONGO_HOST || '',
        MONGO_PORT: process.env.MONGO_PORT || '',
        MONGO_RATING_DATABASE: process.env.MONGO_RATING_DATABASE || '',

        APPLICATION_PORT: process.env.APPLICATION_PORT || '',
    })

const config = generateConfig()

export default config
