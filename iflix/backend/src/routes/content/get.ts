import { Request, Response, NextFunction } from 'express'
import Data from '../../data'


export default (req: Request, res: Response, next: NextFunction) =>
    Data
        .content
        .model
        .find()
        .then(items => res.send(items))
        .catch(next)
