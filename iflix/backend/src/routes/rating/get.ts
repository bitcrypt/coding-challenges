import { Request, Response, NextFunction } from 'express'
import { validateOrReject, IsNumberString } from "class-validator";
import { ContentId } from '../../@domain';
import Data from '../../data';

export type RequestPath = {
    contentId: ContentId
}

export class RequestPathValidator implements RequestPath {

    @IsNumberString()
    contentId!: number;
}


export default async (req: Request, res: Response, next: NextFunction) =>
    validateOrReject(Object.assign(new RequestPathValidator, req.params))
        .catch(next)
        .then(_ => Data
            .scores
            .model
            .find({ contentId: parseInt(req.params.contentId) })
            .then(scores => scores.length === 1
                ? res.send(scores.pop())
                : res.send('NOPE')))
