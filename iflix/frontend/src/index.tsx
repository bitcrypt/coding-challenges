import 'spectre.css'
import 'icons.css'
import './index.css'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './application'
import Data from './data'
import { Provider } from 'react-redux'

const Application = () =>
    <Provider store={Data.store}>
        <App />
    </Provider>

ReactDOM.render(<Application />, document.getElementById('root'));
