import { combineReducers } from 'redux-immutable';
import content from './content/reducers'
import ratings from './ratings/reducers'

export default combineReducers({
    content,
    ratings
})
