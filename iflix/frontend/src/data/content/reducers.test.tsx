import * as Faker from 'faker'
import * as reducers from './reducers'
import { State } from './@records'
import { AsyncCreatorGet, AsyncCreatorError, AsyncCreatorSuccess } from '../@types'

it('Test loadContentStart updates lifecycle ', () => {
    // setup
    const state = State()
    const action: AsyncCreatorGet = {
        type: Faker.random.word(),
        url: Faker.internet.url(),
    }

    // verify
    expect(state.lifecycle.loading).toBe(false)
    expect(state.lifecycle.success).toBe(false)
    expect(state.lifecycle.error).toBe(false)

    // test
    const newState = reducers.loadContentStart(state, action)
    expect(newState.lifecycle.loading).toBe(true)
    expect(newState.lifecycle.success).toBe(false)
    expect(newState.lifecycle.error).toBe(false)
});

it('Test loadContentError updates lifecycle ', () => {
    // setup
    const state = State()
    const action: AsyncCreatorSuccess = {
        type: Faker.random.word(),
        response: {} as any,
        data: [] as any,
        action: {}
    }

    // verify
    expect(state.lifecycle.loading).toBe(false)
    expect(state.lifecycle.success).toBe(false)
    expect(state.lifecycle.error).toBe(false)

    // test
    const newState = reducers.loadContentSuccess(state, action)
    expect(newState.lifecycle.loading).toBe(false)
    expect(newState.lifecycle.success).toBe(true)
    expect(newState.lifecycle.error).toBe(false)
});


it('Test loadContentError updates lifecycle ', () => {
    // setup
    const state = State()
    const action: AsyncCreatorError = {
        type: Faker.random.word(),
        response: {} as any,
        error: {} as any,
        action: {}
    }

    // verify
    expect(state.lifecycle.loading).toBe(false)
    expect(state.lifecycle.success).toBe(false)
    expect(state.lifecycle.error).toBe(false)

    // test
    const newState = reducers.loadContentError(state, action)
    expect(newState.lifecycle.loading).toBe(false)
    expect(newState.lifecycle.success).toBe(false)
    expect(newState.lifecycle.error).toBe(true)
});
