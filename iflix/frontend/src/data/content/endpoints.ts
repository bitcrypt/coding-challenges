export default {
    LOAD_CONTENT: () => `http://${process.env.REACT_APP_RATINGS_HOST}:${process.env.REACT_APP_RATINGS_PORT}/content`
}
