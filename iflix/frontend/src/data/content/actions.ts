import { AsyncActions } from "../@types";

export default {
    load_content: <AsyncActions>{
        START: '@content/LOAD_CONTENT_START',
        SUCCESS: '@content/LOAD_CONTENT_SUCCESS',
        ERROR: '@content/LOAD_CONTENT_ERROR',
    }
}
