import { Record, RecordOf } from 'immutable';
import { Content as _Content, Empty } from "../../@domain";


const defaultValues: _Content = {
    contentId: Empty.NUMBER,
    title: Empty.STRING,
    genre: Empty.STRING,
};


export type Content = RecordOf<_Content>
export const Content: Record.Factory<_Content> = Record(defaultValues);
