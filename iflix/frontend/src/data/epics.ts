import { combineEpics } from 'redux-observable';

import content from './content/epics'
import ratings from './ratings/epics'

export default combineEpics(
    content.getAllContent,

    ratings.getRating,
    ratings.submitRating,
    ratings.refreshRatingAfterSubmit,
)
