// UMD
import * as R from 'ramda'

// types
import { ActionsObservable, StateObservable } from 'redux-observable';
import { AsyncCreatorStart, AsyncCreatorSuccess, AsyncCreatorError, AsyncCreatorGet, AsyncCreatorPost, AjaxResponseWithAction, AjaxErrorWithAction, DependentAction, DependentFunction } from './@types';
import { State } from './@records'

// rxjs
import { ofType } from 'redux-observable';
import { of } from 'rxjs';
import { ajax, AjaxRequest } from 'rxjs/ajax';
import { map, flatMap, catchError, delay, tap } from 'rxjs/operators';
import { Action } from 'rxjs/internal/scheduler/Action';


const getRequestParams = (action: AsyncCreatorGet): AjaxRequest => ({
    method: 'GET',
    url: action.url,
    crossDomain: true,
    headers: {
        'Content-Type': 'application/json',
    },
});


const postRequestParams = (action: AsyncCreatorPost): AjaxRequest => ({
    method: 'POST',
    url: action.url,
    body: JSON.stringify(action.body),
    crossDomain: true,
    headers: {
        'Content-Type': 'application/json',
    },
});


const GET = (
    type: string,
    successHandler: (response: AjaxResponseWithAction) => AsyncCreatorSuccess,
    errorHandler: (error: AjaxErrorWithAction) => AsyncCreatorError,
    successStatus: number = 200) => (action$: ActionsObservable<AsyncCreatorStart<AsyncCreatorGet>>, state$: StateObservable<State>) =>
        action$.pipe(
            ofType(type),
            map(action => ({ params: getRequestParams(action), action: action })),
            flatMap(data =>
                ajax(data.params).pipe(
                    delay(300),
                    catchError(error => of(errorHandler(error))),
                    map(R.ifElse(
                        R.compose(R.equals(successStatus), R.prop('status')),
                        response => successHandler(Object.assign({}, response, {action: data.action})),
                        response => errorHandler(Object.assign({}, response, {action: data.action})),
                    ))
                )
            )
        );


const POST = (
    type: string,
    successHandler: (response: AjaxResponseWithAction) => AsyncCreatorSuccess,
    errorHandler: (error: AjaxErrorWithAction) => AsyncCreatorError,
    successStatus: number = 200) => (action$: ActionsObservable<AsyncCreatorStart<AsyncCreatorPost>>, state$: StateObservable<State>) =>
        action$.pipe(
            ofType(type),
            map(action => ({ params: postRequestParams(action), action: action })),
            flatMap(data =>
                ajax(data.params).pipe(
                    catchError(error => of(errorHandler(error))),
                    map(R.ifElse(
                        R.compose(R.equals(successStatus), R.prop('status')),
                        response => successHandler(Object.assign({}, response, {action: data.action})),
                        response => errorHandler(Object.assign({}, response, {action: data.action})),
                    ))
                )
            )
        );

const DEPENDENT = (
    type: string,
    creator: DependentFunction) => (action$: ActionsObservable<AsyncCreatorStart<AsyncCreatorPost>>, state$: StateObservable<State>) =>
    action$.pipe(
        ofType(type),
        map(creator)
    )

export default {
    GET,
    POST,
    DEPENDENT
}
