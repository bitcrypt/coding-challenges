import creators from './creators'
import epics from './epics'
import store from './store'


export default {
    creators,
    epics,
    store
}
