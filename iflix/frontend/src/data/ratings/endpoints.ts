import { ContentId } from "../@domain";

export default {
    LOAD_RATING_FOR_CONTENT: (contentId: ContentId) => `http://${process.env.REACT_APP_RATINGS_HOST}:${process.env.REACT_APP_RATINGS_PORT}/rating/${contentId}`,
    SUBMIT_RATING_FOR_CONTENT: () => `http://${process.env.REACT_APP_RATINGS_HOST}:${process.env.REACT_APP_RATINGS_PORT}/rating`
}
