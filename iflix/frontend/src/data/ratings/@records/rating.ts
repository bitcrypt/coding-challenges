import { Record, RecordOf } from 'immutable'
import { Lifecycle } from '../../@records/lifecycle'
import { Rating as _Rating, Empty } from "../../@domain"


export type Rating = RecordOf<_Rating>
export const Rating: Record.Factory<_Rating> = Record({
    lifecycle: Lifecycle(),
    contentId: Empty.NUMBER,
    score: Empty.NUMBER,
    count: Empty.NUMBER,
    total: Empty.NUMBER,
});
