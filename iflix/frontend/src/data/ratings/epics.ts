import observables from '../observables'
import actions from './actions'
import creators from './creators'
import { DependentAction, AsyncCreatorSuccess, AsyncCreatorPost } from '../@types';


const getRating = observables.GET(
    actions.load_rating.START,
    creators.load_rating.SUCCESS,
    creators.load_rating.ERROR,
);

const submitRating = observables.POST(
    actions.submit_rating.START,
    creators.submit_rating.SUCCESS,
    creators.submit_rating.ERROR
)

const refreshRatingAfterSubmit = observables.DEPENDENT(
    actions.submit_rating.SUCCESS,
    (action: AsyncCreatorSuccess<any, AsyncCreatorPost<{contentId: number}>>) => creators.load_rating.START(action.action.contentId)
)


export default {
    getRating,
    submitRating,
    refreshRatingAfterSubmit,
}
