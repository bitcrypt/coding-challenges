import { AsyncActions } from "../@types";

export default {
    load_rating: <AsyncActions>{
        START: '@ratings/LOAD_RATING_START',
        SUCCESS: '@ratings/LOAD_RATING_SUCCESS',
        ERROR: '@ratings/LOAD_RATING_ERROR',
    },
    submit_rating: <AsyncActions>{
        START: '@ratings/SUBMIT_RATING_START',
        SUCCESS: '@ratings/SUBMIT_RATING_SUCCESS',
        ERROR: '@ratings/SUBMIT_RATING_ERROR',
    }
}
