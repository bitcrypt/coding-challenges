import { createStore, applyMiddleware, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { epicMiddleware } from './middleware';
import { State } from './@records'

import reducers from './reducers'
import epics from './epics'

export default <Store<State>> createStore(
    reducers,
    State(),
    composeWithDevTools(applyMiddleware(epicMiddleware))
)

epicMiddleware.run(epics as any)
