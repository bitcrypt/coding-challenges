import { useSelector } from 'react-redux'
import { State } from '../data/@records';
import { Genre } from '../data/@domain';

import * as I from 'immutable'
import React from 'react'
import Data from '../data'
import Movie from './Movie'
import LoadingIcon from './LoadingIcon'
import { Content } from '../data/content/@records';

// Returns a curried filter that filters based on Content genre
export const FilterByGenre = (genre: Genre) => (state: State): I.List<Content> =>
    state.content.list.filter(value => value.genre === genre).toList()

// Business logic function to see if content should be loaded.  Checks that it currently
// isn't loading and that it hasn't already loaded
export const ShouldLoadContent = (): boolean => {
    const lifecycle = Data.store.getState().content.lifecycle
    return lifecycle.loading === false && lifecycle.success === false
}

// A composite function that first checks if content is either loading or has loaded,
// and then either loads it or does nothing
export const LoadContent = () =>
    ShouldLoadContent() &&
    Data.store.dispatch(Data.creators.content.load_content.START())


export default () => {
    const lifecycle = useSelector((state: State) => state.content.lifecycle)
    
    const action = useSelector(FilterByGenre('Action'), I.is).toList()
    const horror = useSelector(FilterByGenre('Horror'), I.is).toList()
    const scifi = useSelector(FilterByGenre('Sci-fi'), I.is).toList()

    return (
        <div className="container">

            <div className="columns">
                <div className="p-centered">
                    <img src="logo.png" className="img-fit-contain c-hand" width="200px" onClick={LoadContent}/>
                </div>
            </div>


            <h2>Action</h2>
            <div className="columns col-oneline content-row">
                {lifecycle.loading && <LoadingIcon />}
                {lifecycle.error && <div className='text-error'> Error loading content</div>}
                {action.map(content => <Movie content={content} key={content.contentId}/>)}
            </div>
            <h2>Horror</h2>
            <div className="columns col-oneline content-row">
                {lifecycle.loading && <LoadingIcon />}
                {lifecycle.error && <div className='text-error'> Error loading content</div>}
                {horror.map(content => <Movie content={content} key={content.contentId}/>)}
            </div>
            <h2>Sci-fi</h2>
            <div className="columns col-oneline content-row">
                {lifecycle.loading && <LoadingIcon />}
                {lifecycle.error && <div className='text-error'> Error loading content</div>}
                {scifi.map(content => <Movie content={content} key={content.contentId}/>)}
            </div>
        </div>
    )
}
