import { ContentId, Content, PostRatingBody } from '../data/@domain';
import { State } from '../data/@records';
import { Rating } from '../data/ratings/@records';
import { useSelector } from 'react-redux';

import * as I from 'immutable'
import React from 'react'
import ClassNames from 'classnames'
import Data from '../data'

type Props = {
    active: boolean
    content: Content
    toggle: () => void
}

const GetRating = (contentId: ContentId) => (state: State): Rating =>
    state.ratings.list.get(contentId, Rating())


const SubmitRating = (rating: Rating, score: number) => () =>
    Data.store.dispatch(
        Data.creators.ratings.submit_rating.START(
            rating.contentId,
            {
                contentId: rating.contentId,
                userId: 1111111,
                rating: score
            })
    )


export default ({ active, toggle, content }: Props) => {

    const rating = useSelector(GetRating(content.contentId), I.is)

    return (
        <div className={ClassNames('modal', active && 'active')}>
            <a href="#close" className="modal-overlay" onClick={toggle} />
            <div className="modal-container">
                <div className="modal-header">
                    <a href="#close" className="btn btn-clear float-right" onClick={toggle} ></a>
                    <div className="modal-title h5">How do you rate this movie?</div>
                </div>
                <div className="modal-body">
                    <div className="float-left">
                        <button className="btn btn-action s-circle" onClick={SubmitRating(rating, 1)}>1</button>
                        <button className="btn btn-action s-circle" onClick={SubmitRating(rating, 2)}>2</button>
                        <button className="btn btn-action s-circle" onClick={SubmitRating(rating, 3)}>3</button>
                        <button className="btn btn-action s-circle" onClick={SubmitRating(rating, 4)}>4</button>
                        <button className="btn btn-action s-circle" onClick={SubmitRating(rating, 5)}>5</button>
                    </div>

                    <div className="float-right mr-2">
                        {rating.lifecycle.loading && <div className="loading loading-lg" />}
                        {rating.lifecycle.success && <div>{rating.score.toFixed(2)}</div>}
                        {rating.lifecycle.error && <div className="text-error">Error</div>}
                    </div>

                </div>

                <div className="modal-footer">
                    ...
                </div>
            </div>
        </div>
    )
}
