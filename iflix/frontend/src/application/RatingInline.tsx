import { Content } from '../data/content/@records';
import { useSelector } from 'react-redux'
import { State } from '../data/@records';
import { ContentId } from '../data/@domain';
import { Rating } from '../data/ratings/@records';

import * as I from 'immutable'
import React from 'react'


const GetRating = (contentId: ContentId) => (state: State): Rating =>
    state.ratings.list.get(contentId, Rating())


type Props = {
    content: Content
}


export default ({ content }: Props) => {

    const rating = useSelector(GetRating(content.contentId), I.is)

    return (
        <div className="empty-action">
            {rating.lifecycle.loading && <div className="loading" />}
            {rating.lifecycle.success && rating.score.toFixed(2)}
        </div>
    )
}
