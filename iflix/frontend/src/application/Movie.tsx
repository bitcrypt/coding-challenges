import { Content } from '../data/content/@records/';
import { State } from '../data/@records';
import { ContentId } from '../data/@domain';
import { Rating } from '../data/ratings/@records';

import React from 'react'
import Data from '../data'
import RatingInline from './RatingInline'
import RatingModal from './RatingModal'


// Should this rating be loaded asynchronously?
export const ShouldLoadRating = (contentId: ContentId): boolean => {
    const lifecycle = Data.store.getState().ratings.list.get(contentId, Rating()).lifecycle
    return lifecycle.loading === false && lifecycle.success === false
}


// Get the rating from the store.  Returns a default implementation as a monoid
export const GetRating = (contentId: ContentId) => (state: State): Rating =>
    state.ratings.list.get(contentId, Rating())


// Check if the rating exists, and if it doesn't fetch it asynchronously
export const LoadRating = (contentId: ContentId) => () =>
    ShouldLoadRating(contentId) &&
    Data.store.dispatch(Data.creators.ratings.load_rating.START(contentId))


type Props = {
    content: Content
}


export default ({ content }: Props) => {

    const [modalActive, toggleModal] = React.useState(false)
    const toggle = () => toggleModal(!modalActive);

    return (
        <div className="column col-3">
            <div className="empty c-hand"
                onMouseOver={LoadRating(content.contentId)}
                onClick={toggle}>
                <div className="empty-icon">
                    <i className="icon icon-2x icon-bookmark" />
                </div>
                <p className="empty-subtitle">{content.title.substr(0, 15)}...</p>
                <RatingInline content={content} />
            </div>
            <RatingModal active={modalActive} toggle={toggle} content={content} />
        </div>
    )
}
