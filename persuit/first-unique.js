// Return the first non-repeating character from a lowercase string
// https://codepen.io/mcspud/pen/GbEGLz

const solution = str => {
    Object.prototype.fold = function (f) { return f(this) };

    const val = str
        .split('')
        .reduce((acc, val, idx) => !!acc[val]
            ? Object.assign(acc, {
                [val]: {
                    idx: acc[val].idx,
                    val: val,
                    count: acc[val].count + 1}})
            : Object.assign(acc, {
				[val]: {
                    idx: idx,
                    val: val,
                    count: 1}}),
        {})
        .fold(Object.values)
        .filter(x => x.count === 1)
        .sort((a, b) => a.idx < b.idx ? -1 : 1)
        .shift()
        .val

    delete Object.prototype.fold
    return val
}
		
