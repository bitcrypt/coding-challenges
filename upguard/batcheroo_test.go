package batcheroo

import (
	"context"
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func processFunc(ctx context.Context, job *Job) interface{} {
	// Simulate processing with random timeout
	select {
	case <-time.After(time.Duration(rand.Intn(100)) * time.Millisecond):
		result := fmt.Sprintf("Processed job with ID %d", job.ID)
		fmt.Println(result)
		return result
	case <-ctx.Done():
		result := fmt.Sprintf("Processing of job with ID %d canceled", job.ID)
		fmt.Println(result)
		return result
	}
}

func shutdownFunc(ctx context.Context) {
	fmt.Println("Finished 🤗...")
}

func TestNewBatcheroo(t *testing.T) {
	// Random values so we don't test constants
	chunkSize := rand.Intn(10)
	frequency := rand.Intn(10)
	frequencyDuration := time.Duration(frequency) * time.Second

	ctx := context.Background()
	config := Config{
		chunkSize:      chunkSize,
		frequency:      frequencyDuration,
		processFunc:    processFunc,
		shutdownFunc:   shutdownFunc,
		ctx:            ctx,
		batchProcessor: DefaultBatchProcessor,
	}

	b := NewBatcheroo(config)

	if b.chunkSize != chunkSize {
		t.Errorf("Expected chunkSize to be %d, got %d", chunkSize, b.chunkSize)
	}

	if b.frequency != time.Duration(frequency)*time.Second {
		t.Errorf("Expected frequency to be %d, got %d", frequencyDuration, b.frequency)
	}
}

func TestAddJob(t *testing.T) {
	b := Batcheroo{}
	data := "test data about urmum"
	result := b.AddJob(data)

	if result.Job.ID != 0 {
		t.Errorf("Expected ID to be 0, got %d", result.Job.ID)
	}
}

func TestProcessJobs(t *testing.T) {
	// Define a context with timeout to avoid hanging tests
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Define the chunk size and frequency
	chunkSize := 10
	frequency := 100 * time.Millisecond

	// Create a new Batcheroo instance
	config := Config{
		chunkSize:      chunkSize,
		frequency:      frequency,
		processFunc:    processFunc,
		shutdownFunc:   shutdownFunc,
		ctx:            ctx,
		batchProcessor: DefaultBatchProcessor,
	}
	b := NewBatcheroo(config)

	// Add some jobs
	numJobs := 20
	results := make([]string, numJobs)
	for i := 0; i < numJobs; i++ {
		jobResult := b.AddJob(fmt.Sprintf("Data for job %d", i))
		go func(jobResult JobResult, index int) {
			result := <-jobResult.Job.Result
			// Write the result to the results array so we can inspect it later in a 
			// very side-effecty way
			results[index] = result.(string)
		}(jobResult, i)
	}

	// Start processing jobs
	b.ProcessJobs()

	// Verify that all jobs were processed correctly
	for i := 0; i < numJobs; i++ {
		// Find the index and check that we got what we expected
		expectedResult := fmt.Sprintf("Processed job with ID %d", i)
		if results[i] != expectedResult {
			t.Errorf("Expected result for job %d: %s, got: %s", i, expectedResult, results[i])
		}
	}
}

func TestShutdown(t *testing.T) {
	// Define a context with timeout to avoid hanging tests
	ctx := context.Background()

	// Define a counter variable to be updated by the shutdown function
	var counter int

	// Define the shutdown function
	shutdownFunc := func(ctx context.Context) {
		// Update the counter
		counter = 100
		fmt.Println("Shutdown function executed")
	}

	// Create a new Batcheroo instance
	config := Config{
		chunkSize:      10,
		frequency:      1 * time.Second,
		processFunc:    processFunc,
		shutdownFunc:   shutdownFunc,
		ctx:            ctx,
		batchProcessor: DefaultBatchProcessor,
	}
	b := NewBatcheroo(config)

	// Add some jobs
	for i := 0; i < 10; i++ {
		b.AddJob(fmt.Sprintf("Data for job %d", i))
	}

	// Start processing jobs
	b.ProcessJobs()

	// Manually invoke shutdown method
	b.Shutdown()

	// Wait for the shutdown function to be executed
	select {
	case <-ctx.Done():
		t.Errorf("Shutdown function not executed")
		return
	case <-time.After(3 * time.Second):
		// Check if the counter has been updated
		if counter != 100 {
			t.Errorf("Expected counter to be updated to 100, got %d", counter)
		}
	}
}
