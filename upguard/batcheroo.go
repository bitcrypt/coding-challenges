package batcheroo

import (
	"context"
	"fmt"
	"sync"
	"time"
)

type (
	Job struct {
		ID     int              // Numeric identifier placed on the job.
		Data   interface{}      // Generic data for the Job itself
		Result chan interface{} // Creates a result channel when receiving the processed job
	}

	JobResult struct {
		Job *Job
	}

	Config struct {
		chunkSize      int             // How many jobs to run concurrently
		frequency      time.Duration   // Delay between batches.  Represents a MINIMUM time
		processFunc    ProcessFunc     // The function that operates on an individual job
		shutdownFunc   ShutdownFunc    // Function that is called when the entire process finishes
		ctx            context.Context // Lifecycle Management
		batchProcessor BatchProcessor  // The function that deals with splitting the jobs and concurrent threading
	}

	// Responsible for coordinating the batching of tasks
	// A default is given as a reference
	BatchProcessor func(b Batcheroo)

	// Manually invoked - starts the actual work loop
	ProcessFunc    func(ctx context.Context, job *Job) any

	// Manually invoked cleanup function
	ShutdownFunc   func(ctx context.Context)

	Batcheroo struct {
		chunkSize      int             // The amount of jobs to run concurrently, ie the size of the batch
		frequency      time.Duration   // Frequency at which jobs are processed
		jobs           []*Job          // List of jobs to run.  n jobs will be subdivided into m chunks based on chunkSize
		mutex          sync.Mutex      // Access lock when adding new jobs to the queue.  Allows thread safety
		processFunc    ProcessFunc     // The worker function that does... the work
		shutdownFunc   ShutdownFunc    // Optional cleanup function.
		batchProcessor BatchProcessor  // The function that does the async logic for processing
		ctx            context.Context // Lifecycle handling
	}
)

/**
 * Creates a new Batcheroo struct with associated methods.  This is the entry point to the library.
 *
 * @param chunkSize			The size of each batch to run concurrently.  This will spawn an equivilant number of goroutines
 * @param frequency     Extra time between batch processing.
 * @param processFunc   The function that does the work.
 * @param shutdownFunc  A function that is run at the end of all batches.  Can be implemented as a noop via func() {}
 * @returns Batcheroo
 */
func NewBatcheroo(c Config) Batcheroo {
	return Batcheroo{
		chunkSize:      c.chunkSize,
		frequency:      c.frequency,
		processFunc:    c.processFunc,
		shutdownFunc:   c.shutdownFunc,
		ctx:            c.ctx,
		batchProcessor: c.batchProcessor,
	}
}

/*
 * Adds a new Job to the list. Uses a mutex when accessing the struct to ensure thread safety.
 * The parameter taken is generic and can be anything, including other structs.  Returns a pointer
 *
 * @param data interface   Data specific for that job, and will be available on the struct for the relevant processFunc
 * @returns *Job
 */
func (s *Batcheroo) AddJob(data interface{}) JobResult {
	// Lock the resource for thread saftey
	s.mutex.Lock()
	// Release the resource once function closes
	defer s.mutex.Unlock()

	// Create a new job
	job := &Job{
		ID:     len(s.jobs),
		Data:   data,
		Result: make(chan interface{}, 1),
	}

	// Add the new job to the struct
	s.jobs = append(s.jobs, job)
	// Create a JobResult and return the pointer.
	// TODO: I couldnt get this to construct nicely like
	// JobResult{Job: job}
	jr := JobResult{}
	jr.Job = job
	return jr
}

// This is our main processing function.
/* It is attached to the Batcheroo struct, and then runs the BatchProcessor function
 * WARNING - This does nothing but delegate a processing call.
 *					 This means that mutex locking the struct is left up
 *           to the processor itself.
 */
func (s *Batcheroo) ProcessJobs() {
	// Calculate the batch size
	s.batchProcessor(*s)
}

// Shutdown after all processing completes or context is cancelled.  This will invoke the
// supplied ShutdownFunc as a callback
func (s *Batcheroo) Shutdown() {
	s.shutdownFunc(s.ctx)
}

/* Default processor for jobs.  This can be implemented as a custom depedency depending on
 * your requirement.  It is the callers responsibility for locking the Batcheroo{} struct
 * passed to it,
 */
func DefaultBatchProcessor(b Batcheroo) {

	// Calculate the batch size
	numJobs := len(b.jobs)
	numChunks := (numJobs + b.chunkSize - 1) / b.chunkSize // Ceiling division

	// Loop through the batch and split it into chunks.  I am not sure how to
	// implement golang iterators/generators so we don't need to buffer the
	// whole thing in memory
	for i := 0; i < numChunks; i++ {
		start := i * b.chunkSize
		end := (i + 1) * b.chunkSize
		if end > numJobs {
			end = numJobs
		}

		// Current chunk, with start and end being mutated each iteration
		chunk := b.jobs[start:end]

		// Heres how I understand this to work:
		// - Channels have a capacity, and we set this to our batch/chunk size.
		// - This "capacity" allows that many goroutines to access it at once.
		// - Once that capacity is reached any further access is blocked until a
		//   "slot" or space is freed up by a goroutine releasing its lock
		//
		// This lets us set the maximum concurrency limit (ie goroutines) using this
		// datastructure.
		//
		// This is classic semaphore behaviour so I've called it as such.
		semaphore := make(chan struct{}, b.chunkSize)

		// Now we iterate through the chunks
		for _, job := range chunk {
			// We add an extra entry to the waitgroup to block the next chunk from proceeding
			// wg.Add(1)
			// We create an anonymous function and invoke it immediately with the
			// currently scoped Job pointer
			go func(job *Job) {

				// Acquire a lock on the semaphore channel by piping data into it
				semaphore <- struct{}{}
				// when then function exits open a slot on the semaphore channel by
				// reading from it
				defer func() { <-semaphore }() // Release semaphore

				// Actual business logic
				result := b.processFunc(b.ctx, job)

				// Send result into struct
				job.Result <- result
				// fmt.Printf("routine done\n")
				fmt.Println(job)
			}(job)
		}
		// Delay between processing batches based on frequency
		time.Sleep(b.frequency)
	}
}
