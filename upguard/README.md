## Preface

I'd never used Golang before I did this challenge.  Rather than "picking my best language" I decided to see if I could do a decent job by:
 - using the language preferred on the test
 - teaching myself it 
 - implementing the test

I think this is a better example of ability/capability rather than blindly doing what you already know.

I've heavily commented my code because this shows my thinking.  Be gentle on the implementation 😅


## Problem

I implemented the problem with the following intuition:

- We need to buffer jobs to some queue like structure
- We are going to pluck slices off the queue 
- We are going to process the batch concurrently
- We are not going to get a new batch of jobs until all existing jobs in the current batch are complete

##  Implementation

The challenge contains a few very specific mention of types/interface names.  I spent a while searching around for BatchProcessor in a microbatching library and I found this:

- https://pkg.go.dev/github.com/pbangia/microbatcher#section-readme

I used this as inspiration, however the actual repository has been removed and only the types remain.  There was prior art I saw in this repository, however the BatchProcessor signatures were different so I assumed there is no accepted Go idiom for this.

- https://github.com/fillmore-labs/microbatch

Given that, I know the Test said DONT implement BatchProcessor, however I took a good faith approach here because I couldn't find a standard to work against and implemented my own.  I made the library pluggable so you can implement your own processor if required, and I hope this is an acceptable tradeoff.

## Improvements

There are lots of things I'd like to improve that I've picked up through reading the documentation.  For example:

- As far as I understand it Context is a way to coordinate between goroutines, including lifecycle events.  I'd like to add more of the context lifecycle methods into the Batcheroo struct so we can safely stop the threads etc
    - I didn't do this because I don't have a deep enough feeling of the runtime model yet.  It wasn't clear to me when it would be safe to cancel running jobs for etc, ie can I cancel all jobs from within a Job itself?
- I feel like I should be added JobResults to a channel on Batcheroo instance so it can be processed by a downstream consumer, potentially in parallel.
- It would be nice to have an "incoming" channel that isn't locked, so new jobs can be added to the queue for later processing.  This requires significant uplift to the design so I've left it as is.
- I'm so new to Go the testing could be better.  I was having issues setting up pointers to integers I could pass into concurrent functions to make sure they did what I want them to do.  This is a skill issue, but I'd need a few more hours to do this and it is getting a bit long in the tooth. 

** EDIT **

- I am not well versed in Go error handling and I didnt consider how best to implement this.  Thinking this through I would add an `error channel` to a `JobResult` and shift the `result channel` from `Job` to it.  Ive already submitted the solution so I don't have time now, but hopefully the reviewer catches this before passing judgement.
